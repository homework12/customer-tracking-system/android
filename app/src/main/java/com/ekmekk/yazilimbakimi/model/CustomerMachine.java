package com.ekmekk.yazilimbakimi.model;

public class CustomerMachine {

    private String machineID="";
    private String model="";
    private String installationDate="";
    private String operation="";
    private String maintenanceDate="";
    private String photoID="";
    private String url="";




    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhotoID() {
        return photoID;
    }
    public void setPhotoID(String photoID) {
        this.photoID = photoID;
    }

    public String getMachineID() {
        return machineID;
    }

    public void setMachineID(String machineID) {
        this.machineID = machineID;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMaintenanceDate() {
        return maintenanceDate;
    }

    public void setMaintenanceDate(String maintenanceDate) {
        this.maintenanceDate = maintenanceDate;
    }

    @Override
    public String toString() {
        return "CustomerMachine{" +
                "machineID='" + machineID + '\'' +
                ", model='" + model + '\'' +
                ", installationDate='" + installationDate + '\'' +
                ", operation='" + operation + '\'' +
                ", maintenanceDate='" + maintenanceDate + '\'' +
                ", photoID='" + photoID + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
