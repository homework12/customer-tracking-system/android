package com.ekmekk.yazilimbakimi.model;

public class Address {
    private String province="";
    private String district="";
    private String neighborhood="";
    private String streetAvenue="";
    private String buildingNumber="";
    private String kisaTarif="";

    private String latitude="";
    private String longitude="";




    public Address() {
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {

        this.province = province.toUpperCase();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district.toUpperCase();
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood.toUpperCase();
    }

    public String getStreetAvenue() {
        return streetAvenue;
    }

    public void setStreetAvenue(String streetAvenue) {
        this.streetAvenue = streetAvenue.toUpperCase();
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getKisaTarif() {
        return kisaTarif;
    }

    public void setKisaTarif(String kisaTarif) {
        this.kisaTarif = kisaTarif;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
