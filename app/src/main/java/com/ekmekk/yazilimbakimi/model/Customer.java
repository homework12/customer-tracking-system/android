package com.ekmekk.yazilimbakimi.model;

public class Customer {

    private String id;
    CustomerInformation information;
    CustomerMachine machine;
    CustomerPayment payment;

    public Customer() {
    }

    public CustomerMachine getMachine() {
        return machine;
    }
    public CustomerPayment getPayment() {
        return payment;
    }
    public CustomerInformation getInformation() {
        return information;
    }

    public void setPayment(CustomerPayment payment) {
        this.payment = payment;
    }
    public void setMachine(CustomerMachine machine) {
        this.machine = machine;
    }
    public void setInformation(CustomerInformation information) {
        this.information = information;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }


}
