package com.ekmekk.yazilimbakimi.model;

public class CustomerInformation   extends Address{

    private String name="",phone="",dayLeft="",housePhone="";
    private String informationID;

    public CustomerInformation() {
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDayLeft() {
        return dayLeft;
    }

    public void setDayLeft(String dayLeft) {
        this.dayLeft = dayLeft;
    }


    public String getInformationID() {
        return informationID;
    }

    public void setInformationID(String informationID) {
        this.informationID = informationID;
    }

    public String getHousePhone() {
        return housePhone;
    }

    public void setHousePhone(String housePhone) {
        this.housePhone = housePhone;
    }


    @Override
    public String toString() {
        return "CustomerInformation{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", dayLeft='" + dayLeft + '\'' +
                ", housePhone='" + housePhone + '\'' +
                ", il='" + getProvince() + '\'' +
                ", ilçe='" + getDistrict() + '\'' +
                ", Mahalle='" + getNeighborhood() + '\'' +
                ", sokak='" + getStreetAvenue() + '\'' +
                ", daire no='" + getBuildingNumber() + '\'' +
                ", kisa tarif='" + getKisaTarif() + '\'' +
                ", enlem ='" + getLatitude() + '\'' +
                ", boylam ='" + getLongitude() + '\'' +
                '}';
    }
}
