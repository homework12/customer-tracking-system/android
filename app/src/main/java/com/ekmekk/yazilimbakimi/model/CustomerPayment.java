package com.ekmekk.yazilimbakimi.model;

public class CustomerPayment {

    public String totalPayment="",fromPayment="",installmentDate="",installmentCount="",paymnetType="";


    public CustomerPayment() {
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public String getFromPayment() {
        return fromPayment;
    }

    public void setFromPayment(String fromPayment) {
        this.fromPayment = fromPayment;
    }

    public String getInstallmentDate() {
        return installmentDate;
    }

    public void setInstallmentDate(String installmentDate) {
        this.installmentDate = installmentDate;
    }

    public String getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(String installmentCount) {
        this.installmentCount = installmentCount;
    }


    public String getPaymnetType() {
        return paymnetType;
    }

    public void setPaymnetType(String paymnetType) {
        this.paymnetType = paymnetType;
    }

    @Override
    public String toString() {
        return "CustomerPayment{" +
                "totalPayment='" + totalPayment + '\'' +
                ", fromPayment='" + fromPayment + '\'' +
                ", installmentDate='" + installmentDate + '\'' +
                ", installmentCount='" + installmentCount + '\'' +
                ", paymnetType='" + paymnetType + '\'' +
                '}';
    }
}
