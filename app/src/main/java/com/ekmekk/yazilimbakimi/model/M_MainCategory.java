package com.ekmekk.yazilimbakimi.model;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class M_MainCategory {
    private String name;
    private Intent intent;
    private Drawable drawable;



    public M_MainCategory() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }


    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
