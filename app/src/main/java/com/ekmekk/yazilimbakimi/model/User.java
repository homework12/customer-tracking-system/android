package com.ekmekk.yazilimbakimi.model;

import android.content.Context;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User {

    private String nameSurname;
    private String mail;
    private String companyName;
    private String password;
    private String password2;
    private Context context;
    private  String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    public User(Context context) {
        this.context=context;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }


    public boolean isLogin(){
        if(getMail()==null || getMail().isEmpty()){
            Toast.makeText(context, "EMail Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(getPassword()==null || getPassword().isEmpty()){
            Toast.makeText(context, "Şifre Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

    public boolean isRegister(){
        if(getNameSurname()==null || getNameSurname().isEmpty()){
            Toast.makeText(context, "Ad ve Soyad Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(getMail()==null || getMail().isEmpty()){
            Toast.makeText(context, "EMail Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(!getMail().matches(emailPattern)){
            Toast.makeText(context, "Lütfen Geçerli Bir email Giriniz.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(getCompanyName()==null || getCompanyName().isEmpty()){
            Toast.makeText(context, "Firma İsmi Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(getPassword()==null || getPassword().isEmpty()){
            Toast.makeText(context, "Şifre Boş Bırakılamaz.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(!isValidPassword(getPassword())){
            Toast.makeText(context, "Lütfen güçlü bir şifre oluşturun.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!getPassword().equals(getPassword2())){
            Toast.makeText(context, "Şifreler Aynı Değil", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }

    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}
