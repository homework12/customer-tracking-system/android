package com.ekmekk.yazilimbakimi.model;

import android.content.Intent;

public interface ISendRequestCode  {
    void getResult(int requestCode, int resultCode, Intent data);
}
