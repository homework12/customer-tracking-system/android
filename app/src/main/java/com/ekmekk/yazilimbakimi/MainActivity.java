package com.ekmekk.yazilimbakimi;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.ekmekk.yazilimbakimi.adapter.MainAdapter;
import com.ekmekk.yazilimbakimi.databasemanager.CustomerManager;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Constant;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.ekmekk.yazilimbakimi.model.M_MainCategory;
import com.ekmekk.yazilimbakimi.screen.MapActivity;
import com.ekmekk.yazilimbakimi.screen.PaymentActivity;
import com.ekmekk.yazilimbakimi.screen.ProfilActivity;
import com.ekmekk.yazilimbakimi.screen.stock.StockCategoryActivity;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerAddActivity;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerListActivity;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class MainActivity extends AppCompatActivity {


    public static final String NOTIFICATION_CHANNEL_ID = "10002";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Butler.Instance().Initialize(getApplicationContext(), Constant.SERVER_URL);
        initAdapter();
        
        getPaymentSize();
        findViewById(R.id.textView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             
            }
        });
    }

    private void getPaymentSize() {

        CustomerManager manager=new CustomerManager();
        manager.CustomerGet(Account.getInstance().getUserID(),"2");
        manager.setOnClickListener(new CustomerManager.Response() {
            @Override
            public void Successful(Object o) {
                if(((Collection<? extends CustomerInformation>) o).size()>0){
                   notification();
                }
             
            }

            @Override
            public void Error(String errorType) {

            }
        });



    }

    private void notification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_check)
                        .setContentTitle("Ücret")
                        .setContentText("Ödenmemiş ücretler var");
        int mNotificationId = (int) System.currentTimeMillis();
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);

            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotifyMgr.createNotificationChannel(notificationChannel);
        }
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }


    private void initAdapter() {
        RecyclerView rvGames=findViewById(R.id.rvGames);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        rvGames.setLayoutManager(gridLayoutManager);
        MainAdapter adapter = new MainAdapter(MainActivity.this,getMainCategory());
        rvGames.setAdapter(adapter);
    }


    private ArrayList<M_MainCategory> getMainCategory() {
        ArrayList<M_MainCategory> itemListGames=new ArrayList<>();

        M_MainCategory mainCategory;
        mainCategory=new M_MainCategory();
        mainCategory.setName("Müşteriler");
        mainCategory.setIntent(new Intent(MainActivity.this, CustomerListActivity.class));
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.icon_team));
        itemListGames.add(mainCategory);

        mainCategory=new M_MainCategory();
        mainCategory.setName("Müşteri Ekle");
        mainCategory.setIntent(new Intent(MainActivity.this, CustomerAddActivity.class));
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.icon_team));
        itemListGames.add(mainCategory);

        mainCategory=new M_MainCategory();
        mainCategory.setName("Stok");
        mainCategory.setIntent(new Intent(MainActivity.this, StockCategoryActivity.class));
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.ic_report));
        itemListGames.add(mainCategory);

        mainCategory=new M_MainCategory();
        mainCategory.setName("Haritalar");
        mainCategory.setIntent(new Intent(MainActivity.this, MapActivity.class));
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.icon_map));
        itemListGames.add(mainCategory);

        mainCategory=new M_MainCategory();
        mainCategory.setName("Alınacak Ücretler");
        mainCategory.setIntent(new Intent(MainActivity.this, PaymentActivity.class));
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.icon_team));
        itemListGames.add(mainCategory);


        mainCategory=new M_MainCategory();
        mainCategory.setName("Profil");
        mainCategory.setDrawable(getResources().getDrawable(R.drawable.icon_team));
        mainCategory.setIntent(new Intent(MainActivity.this, ProfilActivity.class));
        itemListGames.add(mainCategory);


        return itemListGames;
    }
}
