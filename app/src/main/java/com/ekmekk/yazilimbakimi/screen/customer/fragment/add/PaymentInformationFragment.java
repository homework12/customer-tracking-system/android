package com.ekmekk.yazilimbakimi.screen.customer.fragment.add;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.model.CustomerMachine;
import com.ekmekk.yazilimbakimi.model.CustomerPayment;

import java.util.Calendar;


public class PaymentInformationFragment extends Fragment {


    private RadioGroup group;
    LinearLayout layout;
    private EditText edtYapilanIsUcreti,edtAlinanUcreti,edtTaksitBaslangicTarihi,edtTaksitSayisi;
    public static PaymentInformationFragment Instance;
    Calendar takvim;
    int yil,ay,gun;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_payment_information2, container, false);

        init(view);
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton){
                    layout.setVisibility(View.GONE);
                }else if(radioGroup.getCheckedRadioButtonId()==R.id.radioButton2){
                    layout.setVisibility(View.VISIBLE);
                }
            }
        });

        takvim = Calendar.getInstance();
        yil = takvim.get(Calendar.YEAR);
        ay = takvim.get(Calendar.MONTH);
        gun = takvim.get(Calendar.DAY_OF_MONTH);

        edtTaksitBaslangicTarihi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                month += 1;
                                edtTaksitBaslangicTarihi.setText(year + "-" + month + "-" + dayOfMonth);
                            }
                        }, yil, ay, gun);
                dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", dpd);
                dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", dpd);
                dpd.show();
            }
        });


        return  view;
    }

    private void init(View view) {
        group=view.findViewById(R.id.radioGroup);
        layout=view.findViewById(R.id.linearTaksit);

        edtYapilanIsUcreti=view.findViewById(R.id.edtYapilanIsUcreti);
        edtAlinanUcreti=view.findViewById(R.id.edtAlinanIsUcreti);
        edtTaksitBaslangicTarihi=view.findViewById(R.id.edtTaksitBaslangicTarihi);
        edtTaksitSayisi=view.findViewById(R.id.edtTaksitSayisi);

        edtTaksitBaslangicTarihi.setFocusable(false);
        PaymentInformationFragment.Instance=this;
    }

    public CustomerPayment payment(){
        CustomerPayment payment=new CustomerPayment();
        payment.setTotalPayment(edtYapilanIsUcreti.getText().toString());
        payment.setFromPayment(edtAlinanUcreti.getText().toString());
        payment.setInstallmentDate(edtTaksitBaslangicTarihi.getText().toString());
        payment.setInstallmentCount(edtTaksitSayisi.getText().toString());
        payment.setPaymnetType(getPaymentType());
        return payment;
    }

    private String getPaymentType(){
        // pesin 0
        // taksit 1;

        if(layout.getVisibility()==View.GONE){
            return "0";
        }else{
            return "1";
        }
    }

}
