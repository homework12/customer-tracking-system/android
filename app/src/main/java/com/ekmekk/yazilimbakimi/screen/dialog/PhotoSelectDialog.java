package com.ekmekk.yazilimbakimi.screen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.jsibbold.zoomage.ZoomageView;

public class PhotoSelectDialog extends Dialog {

    private Context mContext;
    private String photoUrl=null;


    public PhotoSelectDialog(@NonNull Context context) {
        super(context);
        this.mContext=context;
    }

    public PhotoSelectDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected PhotoSelectDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_machine_photo);
        initDialog();
    }

    private void initDialog() {

        ZoomageView imgPhoto=findViewById(R.id.myZoomageView);
        //ConstraintLayout constPhoto=dialog.findViewById(R.id.constPhoto);
        Glide.with(mContext).load(getPhotoUrl()).into(imgPhoto);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);

    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
