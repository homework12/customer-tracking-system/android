package com.ekmekk.yazilimbakimi.screen.dialog;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.databasemanager.StockCategoryManager;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.ekmekk.yazilimbakimi.model.User;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

public class CategoryAddDialog  extends Dialog implements View.OnClickListener {



    private Listener listener;
    public boolean isUpdate=false;
    private EditText edtCategoryName;
    private Stok stok;


    public CategoryAddDialog(@NonNull Context context) {
        super(context);

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_category_add);
        initDialog();
    }



    private void initDialog() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.windowAnimations = R.style.DialogInOut;
        getWindow().setAttributes(lp);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        Button btnAdd =findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        edtCategoryName=findViewById(R.id.edtStokCategoryName);
        findViewById(R.id.btnExit).setOnClickListener(this);
        if(isUpdate){
            btnAdd.setText("Düzenle");
            TextView textView=findViewById(R.id.textView31);
            edtCategoryName.setText(getStok().getCategoryName());
            textView.setText("Kategori Düzenle");
        }
    }






    public void setCategoryAdd(Listener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdd:
                if(isUpdate){
                    updateCategory();
                }else{
                    addCategory();
                }
                break;
            case R.id.btnExit:
                dismiss();
                break;
            default:
        }
    }

    private void updateCategory() {
        Stok stok=new Stok();
        stok.setCategoryName(edtCategoryName.getText().toString());
        stok.setCategoryID(getStok().getCategoryID());
        StockCategoryManager manager=new StockCategoryManager();
        manager.updateCategory(stok);
        manager.setOnClickListener(new StockCategoryManager.Listener() {
            @Override
            public void Successful(Object object) {
                listener.successful(object);
            }

            @Override
            public void Error() {
                listener.error();
            }
        });
    }

    private void addCategory() {
        Stok stok=new Stok();
        stok.setCategoryName(edtCategoryName.getText().toString());
        StockCategoryManager manager=new StockCategoryManager();
        manager.addCategory(stok);
        manager.setOnClickListener(new StockCategoryManager.Listener() {
            @Override
            public void Successful(Object object) {
                listener.successful(object);
            }

            @Override
            public void Error() {
            listener.error();
            }
        });
    }

    public Stok getStok() {
        return stok;
    }

    public void setStok(Stok stok) {
        this.stok = stok;
    }

    public interface Listener{
        void successful(Object o);
        void error();
    }





}
