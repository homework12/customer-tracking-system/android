package com.ekmekk.yazilimbakimi.screen.stock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.databasemanager.ProductManager;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Constant;
import com.ekmekk.yazilimbakimi.helper.OnActivityResult;
import com.ekmekk.yazilimbakimi.helper.Tolbar;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.ekmekk.yazilimbakimi.screen.dialog.PhotoGalleryDialog;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.makeramen.roundedimageview.RoundedImageView;

public class ProductAddActivity extends AppCompatActivity implements View.OnClickListener{

    Stok stock;
    RoundedImageView imgProduct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);
        Tolbar include=new Tolbar(ProductAddActivity.this,"Ürün Ekle");
        include.create();
        init();
        getData();
    }

    private void getData() {

        EditText edtProductExplanation=findViewById(R.id.edtProductExplanation);
        EditText edtProductCount=findViewById(R.id.edtProductCount);
        EditText edtProductName=findViewById(R.id.edtProductName);
        EditText edtBuyingPrice=findViewById(R.id.edtBuyingPrice);
        EditText edtSellingPrice=findViewById(R.id.edtSellingPrice);

        stock.setProductExplanation(edtProductExplanation.getText().toString());
        stock.setCategoryID(Account.getInstance().getStockID());
        stock.setProductCount(edtProductCount.getText().toString());
        stock.setProductName(edtProductName.getText().toString());
        stock.setBuyingPrice(edtBuyingPrice.getText().toString());
        stock.setSellingPrice(edtSellingPrice.getText().toString());
    }

    private void init() {
        findViewById(R.id.btnProductAdd).setOnClickListener(this);
        findViewById(R.id.imgPhotoAdd).setOnClickListener(this);
        stock=new Stok();


        imgProduct=findViewById(R.id.imgProduct);
        Glide.with(this).load("https://img-s2.onedio.com/id-535a5930df0ca4ec5485e6bd/rev-0/raw/s-f1d5e6dc6ad905e27e5c42ae6625486c081165bc.jpg").into(imgProduct);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnProductAdd:
                getData();
                ProductManager manager=new ProductManager();
                manager.addProduct(stock);
                manager.setOnClickListener(new ProductManager.Listener() {
                    @Override
                    public void Successful(Object object) {
                        Toast.makeText(ProductAddActivity.this, "Ürün Eklendi", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void Error() {

                    }
                });

                break;
            case R.id.imgPhotoAdd:

                selectPhotoGalery();
                break;
            default:
        }
    }

    PhotoGalleryDialog dialog;
    private void selectPhotoGalery() {
        dialog=new PhotoGalleryDialog(ProductAddActivity.this);
        dialog.setGallery(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , Constant.GALLERY);
            }
        });
        dialog.setCamera(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePicture.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePicture, Constant.CAMERA);
                }
            }
        });
        dialog.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        dialog.dismiss();
        OnActivityResult result=new OnActivityResult(ProductAddActivity.this);
        result.setData(requestCode,resultCode,data);
        result.setImageOk(new OnActivityResult.response() {
            @Override
            public void ok(String imgString, Bitmap bitmap) {
                stock.setProductImage(imgString);
                imgProduct.setImageBitmap(bitmap);
            }
        });
    }
}
