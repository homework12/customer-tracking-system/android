package com.ekmekk.yazilimbakimi.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.CustomerAdapter;
import com.ekmekk.yazilimbakimi.databasemanager.CustomerManager;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerListActivity;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Collection;

public class PaymentActivity extends AppCompatActivity {

    private ArrayList<CustomerInformation> arrayList=new ArrayList<>();
    private CustomerAdapter adapter;
    SpinKitView prgsBar;
    EditText edtName;
    private ArrayList<CustomerInformation> filteredList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        init();
        initInclude();
        edtName=findViewById(R.id.edtCepPhone);
        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isimArama(edtName.getText().toString());
            }
        });
    }

    private void isimArama(String text){
        filteredList.clear();

        for(CustomerInformation item : arrayList){
            if(item.getName().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(item);
            }
        }
        adapter.filterList(filteredList);
    }

    @SuppressLint("SetTextI18n")
    private void initInclude() {
        View view=findViewById(R.id.includeTopMenu);
        ImageView imgBack=view.findViewById(R.id.imgBack);
        TextView txtActivityName=view.findViewById(R.id.txtActivityName);
        txtActivityName.setText("Alınacak Ücretler");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void init() {

        RecyclerView rvGames=findViewById(R.id.rvPayment);
        LinearLayoutManager manager = new LinearLayoutManager(PaymentActivity.this);
        rvGames.setLayoutManager(manager);
        adapter = new CustomerAdapter(PaymentActivity.this,arrayList);
        rvGames.setAdapter(adapter);
        getCustomer();
        prgsBar=findViewById(R.id.spin_kit);

    }

    private void getCustomer() {

        CustomerManager manager=new CustomerManager();
        manager.CustomerGet(Account.getInstance().getUserID(),"2");
        manager.setOnClickListener(new CustomerManager.Response() {
            @Override
            public void Successful(Object o) {
                arrayList.clear();
                prgsBar.setVisibility(View.GONE);
                arrayList.addAll((Collection<? extends CustomerInformation>) o);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void Error(String errorType) {

            }
        });



    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getCustomer();
    }
}
