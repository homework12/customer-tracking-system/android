package com.ekmekk.yazilimbakimi.screen.stock;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.StokListAdapter;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Tolbar;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.github.ybq.android.spinkit.SpinKitView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StockListActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<Stok> arrayList=new ArrayList<>();
    StokListAdapter adapter;
    SpinKitView prgsBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_list);
        Tolbar tolbar=new Tolbar(StockListActivity.this,"Ürün Listesi");
        tolbar.create();
        initAdapter();
        initView();
        getStockList();
    }

    private void initView() {
        findViewById(R.id.imgAdd).setOnClickListener(this);
        prgsBar=findViewById(R.id.spin_kit);

    }

    private void initAdapter() {
        RecyclerView rvStockList=findViewById(R.id.rvStokList);
        LinearLayoutManager manager=new LinearLayoutManager(getApplicationContext());
        rvStockList.setLayoutManager(manager);
         adapter=new StokListAdapter(StockListActivity.this,arrayList);
        rvStockList.setAdapter(adapter);
    }

    private void getStockList() {
        arrayList.clear();
        Log.e("kadir",Account.getInstance().getStockID());
        ButlerRequest request = Butler.Instance().Path("product/list").GET(Account.getInstance().getStockID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("urun listesi",data);
                try {
                    JSONObject object=new JSONObject(data);
                    JSONArray jsonObject=object.getJSONArray("list");
                    for (int i = 0; i < jsonObject.length(); i++) {
                        JSONObject object1=jsonObject.getJSONObject(i);
                        Stok stok=new Stok();
                        stok.setProductName(object1.getString("productName"));
                        stok.setStockProductCount(object1.getString("productCount")+" Adet");
                        stok.setProductID(object1.getString("productID"));
                        arrayList.add(stok);
                    }
                    adapter.notifyDataSetChanged();
                    prgsBar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgAdd:
                startActivity(new Intent(getApplicationContext(),ProductAddActivity.class));
                break;
            default:
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getStockList();
    }
}
