package com.ekmekk.yazilimbakimi.screen.customer.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.ViewPagerAdapter;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.information.MachineInformationFragment;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.information.PaymentInformationFragment;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.information.PhotoFragment;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.google.android.material.tabs.TabLayout;

import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomerInformationActivity extends AppCompatActivity implements View.OnClickListener {




    String il,ilce,mahalle,adress,kisaTarif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_information);
        init();
        setupViewPager();
        getInformation();

    }

    private void getInformation() {
        Log.e("customerID",Account.getInstance().getCustomerID());
        ButlerRequest request = Butler.Instance().Path("customer").GET(Account.getInstance().getCustomerID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("information",data);
                TextView txtName=findViewById(R.id.textView21);
                TextView txtNumber=findViewById(R.id.textView24);
                TextView txtAdress=findViewById(R.id.txtAdres);
                try {
                    JSONArray array=new JSONArray(data);
                    JSONObject object=array.getJSONObject(0);
                    txtName.setText(object.getString("customerFullname"));
                    txtNumber.setText(object.getString("customerCellphone"));
                    il=object.getString("city");
                    ilce=object.getString("district");
                    mahalle=object.getString("neighborhood");
                    adress=object.getString("street");
                    kisaTarif=object.getString("direction");
                    txtAdress.setText(object.getString("city")+" / "+object.getString("district"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void init() {

        int[] id=new int[]{R.id.txtAdres,R.id.imgBack};
        for(int i : id){
            findViewById(i).setOnClickListener(this);
        }

        final RoundedImageView imgPhoto=findViewById(R.id.imgPhoto);
        // imgPhoto kareye çeviriyor
        ViewTreeObserver vto = imgPhoto.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imgPhoto.getViewTreeObserver().removeOnPreDrawListener(this);
                imgPhoto.getLayoutParams().width=imgPhoto.getMeasuredHeight();
                imgPhoto.requestLayout();
                //Log.e("image","genişlik="+imgPhoto.getWidth()+" Yukseklik"+imgPhoto.getHeight());
                return true;
            }
        });
        Glide.with(this).load("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg").into(imgPhoto);

    }
    private void setupViewPager() {

        ViewPager viewPager =  findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MachineInformationFragment(), "Makine Bilgileri");
        adapter.addFrag(new PaymentInformationFragment(), "Ödeme Bilgileri");
        //adapter.addFrag(new PhotoFragment(), "Fotoğraflar");
        viewPager.setAdapter(adapter);
        //Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins_bold);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private void adressDialog() {
        final Dialog dialog=new Dialog(CustomerInformationActivity.this);
        dialog.setContentView(R.layout.dialog_adres);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogInOut;
        Button btnExit=dialog.findViewById(R.id.btnExit);
        TextView txtIl,txtIlce,txtMahalle,txtAdress,txtKısaTarif;
        txtIl=dialog.findViewById(R.id.textView38);
        txtIlce=dialog.findViewById(R.id.textView39);
        txtMahalle=dialog.findViewById(R.id.textView40);
        txtAdress=dialog.findViewById(R.id.textView41);
        txtKısaTarif=dialog.findViewById(R.id.textView42);

        txtIl.setText(il);
        txtIlce.setText(ilce);
        txtMahalle.setText(mahalle);
        txtAdress.setText(adress);
        txtKısaTarif.setText(kisaTarif);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtAdres:
                adressDialog();
                break;
            case R.id.imgBack:
                finish();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(new Intent(getApplicationContext(),CustomerInformationActivity.class));
    }
}


