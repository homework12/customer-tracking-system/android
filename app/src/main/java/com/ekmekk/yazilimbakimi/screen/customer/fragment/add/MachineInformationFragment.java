package com.ekmekk.yazilimbakimi.screen.customer.fragment.add;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.model.CustomerMachine;

import java.util.Calendar;


public class MachineInformationFragment extends Fragment {

    private EditText edtMontajTarihi,edtCihazModeli,edtYapilanIslem,edtBakimTarihi;
    public static MachineInformationFragment Instance;
    int yil,ay,gun;
    Calendar takvim;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_machine_information2, container, false);
        init(view);

         takvim = Calendar.getInstance();
         yil = takvim.get(Calendar.YEAR);
         ay = takvim.get(Calendar.MONTH);
         gun = takvim.get(Calendar.DAY_OF_MONTH);
         edtBakimTarihi.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 DatePickerDialog dpd = new DatePickerDialog(getContext(),
                         new DatePickerDialog.OnDateSetListener() {
                             @Override
                             public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                 month += 1;
                                 edtBakimTarihi.setText(year + "-" + month + "-" + dayOfMonth);
                             }
                         }, yil, ay, gun);
                 dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", dpd);
                 dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", dpd);
                 dpd.show();
             }
         });
         edtMontajTarihi.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 DatePickerDialog dpd = new DatePickerDialog(getContext(),
                         new DatePickerDialog.OnDateSetListener() {
                             @Override
                             public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                 month += 1;
                                 edtMontajTarihi.setText(year + "-" + month + "-" + dayOfMonth);
                             }
                         }, yil, ay, gun);
                 dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", dpd);
                 dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", dpd);
                 dpd.show();
             }
         });
        return view;
    }

    private void init(View view) {
        edtMontajTarihi=view.findViewById(R.id.edtMontajTarihi);
        edtCihazModeli=view.findViewById(R.id.edtCihazModeli);
        edtYapilanIslem=view.findViewById(R.id.edtYapilanIslem);
        edtBakimTarihi=view.findViewById(R.id.edtBakimTarihi);

        edtMontajTarihi.setFocusable(false);
        edtBakimTarihi.setFocusable(false);
        MachineInformationFragment.Instance=this;
    }
    public CustomerMachine machine(){
        CustomerMachine machine=new CustomerMachine();
        machine.setInstallationDate(edtMontajTarihi.getText().toString());
        machine.setMaintenanceDate(edtBakimTarihi.getText().toString());
        machine.setOperation(edtYapilanIslem.getText().toString());
        machine.setModel(edtCihazModeli.getText().toString());
        return machine;
    }

}
