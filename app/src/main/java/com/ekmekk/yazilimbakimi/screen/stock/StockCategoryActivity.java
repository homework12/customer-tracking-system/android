package com.ekmekk.yazilimbakimi.screen.stock;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.StokCategoryAdapter;
import com.ekmekk.yazilimbakimi.databasemanager.StockCategoryManager;
import com.ekmekk.yazilimbakimi.helper.Tolbar;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.ekmekk.yazilimbakimi.screen.dialog.CategoryAddDialog;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Collection;


public class StockCategoryActivity extends AppCompatActivity  implements View.OnClickListener{


    ArrayList<Stok> stokArrayList=new ArrayList<>();
    StokCategoryAdapter adapter;
    SpinKitView prgsBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stok);
        Tolbar include=new Tolbar(StockCategoryActivity.this,"Stok Categori");
        include.create();
        initView();
        initAdapter();
        getStokCategory();


    }

    private void initView() {
        findViewById(R.id.imgAdd).setOnClickListener(this);
        prgsBar=findViewById(R.id.spin_kit);
    }

    private void initAdapter() {
        RecyclerView rvList=findViewById(R.id.rvStokCategory);
        final LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        rvList.setLayoutManager(manager);
        adapter= new StokCategoryAdapter(StockCategoryActivity.this,stokArrayList);
        rvList.setAdapter(adapter);
        adapter.setDialogResponse(new StokCategoryAdapter.Listener() {
            @Override
            public void edit(Stok stok) {
                final CategoryAddDialog dialog=new CategoryAddDialog(StockCategoryActivity.this);
                dialog.isUpdate=true;
                dialog.setStok(stok);
                dialog.setCategoryAdd(new CategoryAddDialog.Listener() {
                    @Override
                    public void successful(Object o) {
                        Toast.makeText(StockCategoryActivity.this, "Kategori Güncellendi.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        getStokCategory();
                    }

                    @Override
                    public void error() {
                        Toast.makeText(StockCategoryActivity.this, "Hata Oluştu.", Toast.LENGTH_SHORT).show();
                        dialog.show();
                    }
                });
                dialog.show();
            }

            @Override
            public void delete(Stok stok) {
                StockCategoryManager manager1=new StockCategoryManager();
                manager1.deleteCategory(stok);
                manager1.setOnClickListener(new StockCategoryManager.Listener() {
                    @Override
                    public void Successful(Object object) {
                        Toast.makeText(StockCategoryActivity.this, "Kategori Silindi", Toast.LENGTH_SHORT).show();
                        getStokCategory();

                    }

                    @Override
                    public void Error() {
                        Toast.makeText(StockCategoryActivity.this, "hata oluştu", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });

    }
    private void getStokCategory() {
        StockCategoryManager manager=new StockCategoryManager();
        manager.getCategoryList();
        manager.setOnClickListener(new StockCategoryManager.Listener() {
            @Override
            public void Successful(Object object) {
                stokArrayList.clear();
                stokArrayList.addAll((Collection<? extends Stok>) object);
                prgsBar.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void Error() {
                prgsBar.setVisibility(View.GONE);
            }
        });

    }


    private void addCategory() {
        final CategoryAddDialog dialog=new CategoryAddDialog(StockCategoryActivity.this);
        dialog.setCategoryAdd(new CategoryAddDialog.Listener() {
            @Override
            public void successful(Object o) {
                Toast.makeText(StockCategoryActivity.this, "Kategori Eklendi.", Toast.LENGTH_SHORT).show();
                getStokCategory();
                dialog.dismiss();
            }

            @Override
            public void error() {
                Toast.makeText(StockCategoryActivity.this, "Hata Oluştu.", Toast.LENGTH_SHORT).show();
                dialog.show();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgAdd:
                addCategory();
                break;
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getStokCategory();
    }
}