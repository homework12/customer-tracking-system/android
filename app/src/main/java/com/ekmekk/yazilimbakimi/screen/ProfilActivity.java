package com.ekmekk.yazilimbakimi.screen;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfilActivity extends AppCompatActivity implements View.OnClickListener {

    RoundedImageView imgProfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        init();
        getUserProfil();
    }

    private void getUserProfil() {
        ButlerRequest request = Butler.Instance().Path("user").GET(Account.getInstance().getUserID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("user",data);
                try {
                    JSONObject object=new JSONObject(data);
                    JSONObject jsonObject=object.getJSONObject("user");
                    TextView edtName=findViewById(R.id.textView2);
                    TextView edtCompanyName=findViewById(R.id.textView3);
                    TextView edtCustomerList=findViewById(R.id.textView8);
                    TextView edtProdoctCount=findViewById(R.id.textView11);
                    TextView edtTotalPayment=findViewById(R.id.textView10);

                    edtName.setText(jsonObject.getString("fullname"));
                    edtCompanyName.setText(jsonObject.getString("companyName"));
                    edtCustomerList.setText(jsonObject.getString("customerCount"));
                    edtProdoctCount.setText(jsonObject.getString("totalProductCount"));
                    edtTotalPayment.setText("0 Tl");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void init() {

        int[] id=new int[]{R.id.imgBack,R.id.cardExit};
        for (int i : id) {
            findViewById(i).setOnClickListener(this);
        }

        imgProfil=findViewById(R.id.imgPhoto);


       // RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.kadir).error(R.drawable.kadir);
        Glide.with(this).load("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg").into(imgProfil);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                finish();
                break;
            case R.id.cardExit:
                Account.getInstance().userLogout();
                Intent intent=new Intent(this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }
}
