package com.ekmekk.yazilimbakimi.screen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.CustomerAdapter;
import com.ekmekk.yazilimbakimi.databasemanager.CustomerManager;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Alert;
import com.ekmekk.yazilimbakimi.helper.LocationService;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerInformationActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collection;


public class MapActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private ArrayList<CustomerInformation> arrayList=new ArrayList<>();
    private CustomerAdapter adapter;
    GoogleMap map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initInclude();
        SupportMapFragment mapFragment=(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



    }

    @SuppressLint("SetTextI18n")
    private void initInclude() {
        View view=findViewById(R.id.include);
        ImageView imgBack=view.findViewById(R.id.imgBack);
        TextView txtActivityName=view.findViewById(R.id.txtActivityName);
        txtActivityName.setText("Haritalar");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;


       LocationService  locationService=new LocationService(getApplicationContext());



        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationService.getEnlem(), locationService.getBoylam()), 25.0f));
        map.setMyLocationEnabled(true);
        getCustomer();
       map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
           @Override
           public boolean onMarkerClick(Marker marker) {
               Show(marker.getTitle());
               return false;
           }
       });
    }

    private void getCustomer() {

        CustomerManager manager=new CustomerManager();
        manager.CustomerGet(Account.getInstance().getUserID(),"3");
        manager.setOnClickListener(new CustomerManager.Response() {
            @Override
            public void Successful(Object o) {
                arrayList.addAll((Collection<? extends CustomerInformation>) o);
                addMarker(arrayList);
            }

            @Override
            public void Error(String errorType) {

            }
        });



    }

    private void Show(String title){
        for (final CustomerInformation customer : arrayList){
            if(title.equals(customer.getName())){
                final Alert alert=new Alert(MapActivity.this);
                alert.setTitle(customer.getName());
                alert.setMessage("Bakıma kalan gün "+customer.getDayLeft());
                alert.setNegativeTitle("İptal");
                alert.setPositveButton("Bilgi",new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Account.getInstance().setCustomerID(customer.getInformationID());
                        startActivity(new Intent(getApplicationContext(), CustomerInformationActivity.class));
                        alert.dismiss();

                    }
                });
                alert.show();
                break;
            }


        }
    }
    private void addMarker(ArrayList<CustomerInformation> arrayList) {
        for (CustomerInformation customer : arrayList){
            LatLng lng=new LatLng(Double.parseDouble(customer.getLatitude()),Double.parseDouble(customer.getLongitude()));
            map.addMarker(new MarkerOptions().position(lng).title(customer.getName()));

        }
    }



}
