package com.ekmekk.yazilimbakimi.screen.customer.fragment.add;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.LocationService;
import com.ekmekk.yazilimbakimi.helper.Permisson;
import com.ekmekk.yazilimbakimi.helper.SearchPlaces;
import com.ekmekk.yazilimbakimi.model.Address;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.github.vacxe.phonemask.PhoneMaskManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class InformationFragment extends Fragment implements View.OnClickListener {


    private Context mContext;
    @Override
    public void onAttach(@NonNull Context context) {
        this.mContext=context;
        super.onAttach(context);
    }

    @SuppressLint("StaticFieldLeak")
    public static InformationFragment Instance;
    Address mAddres=new Address();

    private EditText edtName,edtCephone,edtHousePhone,edtKisaTarih,edtDaireNo;
    private PhoneMaskManager phoneCep=new PhoneMaskManager();
    private PhoneMaskManager phoneHouse=new PhoneMaskManager();
    private AutoCompleteTextView edtIL,edtIlce,edtMahalle,edtSokakCadde;
    private String ilceKey="-1";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_information, container, false);
        init(view);
        getIL(view);

        edtIlce.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                getIlce(SearchPlaces.getIlce(getContext(),edtIL.getText().toString().trim()));
            }
        });
        edtMahalle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                getMahalle(SearchPlaces.getMahalle(getContext(),edtIlce.getText().toString().trim()));
            }
        });
        return view;
    }

    private void init(View view) {
        int[] id=new int[]{R.id.btnLocation,R.id.btnMap};
        for (int i : id) {
            view.findViewById(i).setOnClickListener(this);
        }

        edtName=view.findViewById(R.id.edtName);
        edtCephone=view.findViewById(R.id.edtCepPhone);
        edtHousePhone=view.findViewById(R.id.edtHousePhone);
        edtIL =view.findViewById(R.id.edtIl);
        edtIlce =view.findViewById(R.id.edtIlce);
        edtMahalle =view.findViewById(R.id.edtMahalle);
        edtSokakCadde =view.findViewById(R.id.edtSokakCadde);
        edtDaireNo=view.findViewById(R.id.edtDaireNo);
        edtKisaTarih=view.findViewById(R.id.edtKisaTarih);



        edtIL.setOnClickListener(this);
        edtIlce.setOnClickListener(this);

        InformationFragment.Instance = this;
        setPhone();
    }
    private void setPhone() {
        phoneCep.withMask(" (###)  ### - ## - ##");
        phoneCep.withRegion("+90");
        phoneCep .bindTo(edtCephone);

        phoneHouse.withMask(" (###)  ### - ## - ##");
        phoneHouse.withRegion("+0");
        phoneHouse .bindTo(edtHousePhone);
    }
    private void setAddress(Address address){
        if(address!=null){
            edtIL.setText(address.getProvince());
            edtIlce.setText(address.getDistrict());
            edtMahalle.setText(address.getNeighborhood());
            edtSokakCadde.setText(address.getStreetAvenue());
        }
    }
    private void getIL(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (getContext(),android.R.layout.select_dialog_item, SearchPlaces.getIL(getContext()));

        edtIL.setThreshold(1);
        edtIL.setAdapter(adapter);

    }
    private void getIlce(ArrayList<String> ilce) {
        if(ilce.size()!=0 && !ilce.get(ilce.size()-1).equals("-1")){
            ilceKey=ilce.get(ilce.size()-1);
            ilce.remove(ilce.size()-1);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (getContext(),android.R.layout.select_dialog_item, ilce);
        //Getting the instance of AutoCompleteTextView

        edtIlce.setThreshold(1);//will start working from first character
        edtIlce.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView

    }
    private void getMahalle(ArrayList<String> Firmalar) {

        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (getContext(),android.R.layout.select_dialog_item, Firmalar);
        //Getting the instance of AutoCompleteTextView

        edtMahalle.setThreshold(1);//will start working from first character
        edtMahalle.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView

    }
    private void initMap(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_map);////your custom content
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogInOut;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        
        MapView mMapView = (MapView) dialog.findViewById(R.id.mapView);
        MapsInitializer.initialize(getActivity());
        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                LocationService  locationService=new LocationService(getContext());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationService.getEnlem(), locationService.getBoylam()), 25.0f));
                googleMap.setMyLocationEnabled(true);

                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            double latitude = latLng.latitude;
                            double longitude = latLng.longitude;

                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(latLng);
                            markerOptions.title("Adres Konumu");
                            googleMap.clear();
                            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                            googleMap.addMarker(markerOptions);
                            Geocoder gCoder = new Geocoder(mContext);
                            List<android.location.Address> addresses;
                            try {
                                addresses = gCoder.getFromLocation(latitude, longitude, 1);
                                if (addresses != null && addresses.size() > 0) {
                                    android.location.Address address = addresses.get(0);
                                    if(addresses.get(0).getAdminArea()!=null){
                                        mAddres.setProvince(addresses.get(0).getAdminArea());
                                    }
                                    if(addresses.get(0).getSubAdminArea()!=null){
                                        mAddres.setDistrict(addresses.get(0).getSubAdminArea());
                                    }
                                    if(addresses.get(0).getSubLocality()!=null){
                                        mAddres.setNeighborhood(addresses.get(0).getSubLocality());
                                    }
                                    if(addresses.get(0).getThoroughfare()!=null){
                                        mAddres.setStreetAvenue(addresses.get(0).getThoroughfare());
                                    }
                                }
                            } catch (IOException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                         googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        LatLng latLng = new LatLng(latitude, longitude);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                        googleMap.setOnMyLocationChangeListener(null);


                    }
                });
            }
        });
        dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAddress(mAddres);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public CustomerInformation information(){
        CustomerInformation information=new CustomerInformation();
        information.setName(edtName.getText().toString());
        information.setPhone(phoneCep.getPhone());
        information.setHousePhone(phoneHouse.getPhone());
        information.setProvince(edtIL.getText().toString());
        information.setDistrict(edtIlce.getText().toString());
        information.setNeighborhood(edtMahalle.getText().toString());
        information.setStreetAvenue(edtSokakCadde.getText().toString());
        information.setBuildingNumber(edtDaireNo.getText().toString());
        information.setKisaTarif(edtKisaTarih.getText().toString());
        information.setLatitude(mAddres.getLatitude());
        information.setLongitude(mAddres.getLongitude());
        return information;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLocation:
                if(Permisson.isLocation(getContext())){
                    LocationService service=new LocationService(mContext);
                    mAddres=service.getAddress();
                    setAddress(mAddres);
                    service.stopUsingGPS();
                }
                break;
            case R.id.btnMap:
               if(Permisson.isLocation(getContext())){
                   initMap();
               }
                break;
            default:
        }
    }


}
// TODO: 10.10.2020 il ilçe mahalle hızlandırılcak