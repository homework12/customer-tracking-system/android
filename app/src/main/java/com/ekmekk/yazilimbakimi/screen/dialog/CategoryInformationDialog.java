package com.ekmekk.yazilimbakimi.screen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

public class CategoryInformationDialog  extends Dialog  implements View.OnClickListener{

    public CategoryInformationDialog(@NonNull Context context) {
        super(context);
    }

    private Listener listener;

    private Stok stok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_category_information);
        initDialog();
    }



    private void initDialog() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.windowAnimations = R.style.DialogInOut;
        getWindow().setAttributes(lp);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        findViewById(R.id.btnEdit).setOnClickListener(this);
        findViewById(R.id.btnExit).setOnClickListener(this);
        findViewById(R.id.btnDelete).setOnClickListener(this);

        TextView txtCategoryName=findViewById(R.id.textView17);
        TextView txtProductCount=findViewById(R.id.textView18);
        TextView txtCreationDate=findViewById(R.id.textView19);

        txtCategoryName.setText(getStok().getCategoryName());
        txtProductCount.setText(getStok().getStockProductCount());
        txtCreationDate.setText(getStok().getCreationDate());




    }





    public void setOnClickListener(Listener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEdit:
                listener.edit();
                break;
            case R.id.btnDelete:
                listener.delete();
                break;
            case R.id.btnExit:
                dismiss();
                break;
            default:
        }
    }

    public Stok getStok() {
        return stok;
    }

    public void setStok(Stok stok) {
        this.stok = stok;
    }

    public interface Listener{
        void edit();
        void delete();
    }





}
