package com.ekmekk.yazilimbakimi.screen.customer.fragment.information;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.MachinePhotosAdapter;
import com.ekmekk.yazilimbakimi.helper.Constant;
import com.ekmekk.yazilimbakimi.helper.OnActivityResult;
import com.ekmekk.yazilimbakimi.helper.Permisson;
import com.ekmekk.yazilimbakimi.model.CustomerMachine;
import com.ekmekk.yazilimbakimi.screen.dialog.PhotoGalleryDialog;
import com.ekmekk.yazilimbakimi.screen.dialog.PhotoSelectDialog;


import java.util.ArrayList;


public class PhotoFragment extends Fragment implements MachinePhotosAdapter.ListAdapterListener {



    private Context mContext;
    PhotoGalleryDialog dialog;
    @Override
    public void onAttach(@NonNull Context context) {
        this.mContext=context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_photo, container, false);
        initAdapter(view);
        return view;
    }

    private void initAdapter(View view) {
        MachinePhotosAdapter adapter;
        RecyclerView rvPhotos;
        rvPhotos=view.findViewById(R.id.rvMachinePhotos);
        adapter=new MachinePhotosAdapter(getContext(),getPhotos(),this);
        rvPhotos.setAdapter(adapter);
        rvPhotos.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }
    private ArrayList<CustomerMachine> getPhotos() {
        ArrayList<CustomerMachine> arrayPhoto=new ArrayList();
        CustomerMachine photos=new CustomerMachine();
        photos.setUrl("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg");
        arrayPhoto.add(photos);

        photos=new CustomerMachine();
        photos.setUrl("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg");
        arrayPhoto.add(photos);

        photos=new CustomerMachine();
        photos.setUrl("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg");
        arrayPhoto.add(photos);


        photos=new CustomerMachine();
        photos.setUrl("https://www.icemodelmgmt.com/upload/cache/upload/ckfinder/files/aleksandr-erkek/aleksandr-erkek-model-kapak-540x716.jpg");
        arrayPhoto.add(photos);

        return arrayPhoto;
    }


    @Override
    public void selectPhoto(CustomerMachine photos) {
        PhotoSelectDialog dialog=new PhotoSelectDialog(getContext());
        dialog.setPhotoUrl(photos.getUrl());
        dialog.show();

    }
    @Override
    public void addPhoto() {
        if(Permisson.isCamera(mContext)){
            dialog=new PhotoGalleryDialog(getContext());
            dialog.setGallery(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , Constant.GALLERY);
                }
            });
            dialog.setCamera(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePicture.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivityForResult(takePicture, Constant.CAMERA);
                    }
                }
            });
            dialog.show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        dialog.dismiss();
        OnActivityResult result=new OnActivityResult(getContext());
        result.setData(requestCode,resultCode,data);
        result.setImageOk(new OnActivityResult.response() {
            @Override
            public void ok(String imgString, Bitmap bitmap) {
                Toast.makeText(mContext, "kadirrrr", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
