package com.ekmekk.yazilimbakimi.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.MainActivity;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Constant;
import com.ekmekk.yazilimbakimi.model.User;
import com.ekmekk.yazilimbakimi.databasemanager.UserManager;
import com.furkanbahadirozdemir.butler.Butler;


public class LoginActivity extends AppCompatActivity  implements View.OnClickListener{

    View viewRegister;
    ConstraintLayout viewLogin;
    ProgressBar prgsBarLogin,prgsBarRegister;
    CardView btnLogin,btnRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Account.getInstance().init(LoginActivity.this);
        if(Account.getInstance().isUserLogin()){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }
        init();


    }
    private void init() {
        Butler.Instance().Initialize(getApplicationContext(), Constant.SERVER_URL);

        int[] id=new int[]{R.id.cardLogin,R.id.txtRegister};
        for (int i : id ){
            findViewById(i).setOnClickListener(this);
        }

        // rregister
        viewRegister=findViewById(R.id.includeRegister);
        TextView txtLogin=findViewById(R.id.txtLogin);
        txtLogin.setOnClickListener(this);
        btnRegister=viewRegister.findViewById(R.id.cardRegister);
        btnRegister.setOnClickListener(this);

        //login
        viewLogin=findViewById(R.id.constLogin);

        prgsBarLogin=findViewById(R.id.spin_kit);
        prgsBarRegister=viewRegister.findViewById(R.id.spin_kit);

        btnLogin=findViewById(R.id.cardLogin);

    }
    private void setNull() {
        EditText edtNameSurname=viewRegister.findViewById(R.id.edtNameSurname);
        EditText edtMail=viewRegister.findViewById(R.id.edtMail);
        EditText edtCompanyName=viewRegister.findViewById(R.id.edtCompanyName);
        EditText edtPassword=viewRegister.findViewById(R.id.edtPassword);
        EditText edtPassword2=viewRegister.findViewById(R.id.edtPassword2);
        edtNameSurname.setText("");
        edtMail.setText("");
        edtCompanyName.setText("");
        edtPassword.setText("");
        edtPassword2.setText("");

    }
    private void showView() {
        TextView txtTitle=findViewById(R.id.txtTitle);
        if(viewLogin.getVisibility()==View.VISIBLE){
            viewLogin.setVisibility(View.GONE);
            viewRegister.setVisibility(View.VISIBLE);
            txtTitle.setText("Kayıt Ol");
        }else {
            viewLogin.setVisibility(View.VISIBLE);
            viewRegister.setVisibility(View.GONE);
            txtTitle.setText("Giriş");
        }

    }
    private void login(User user) {

        btnLogin.setVisibility(View.INVISIBLE);
        prgsBarLogin.setVisibility(View.VISIBLE);
        UserManager manager=new UserManager();
        manager.isLogin(user);
        manager.loginResponse(new UserManager.ListenerLogin() {
            @Override
            public void isSuccessful(String userID) {
                Account.getInstance().setUserID(userID);
                Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                btnLogin.setVisibility(View.VISIBLE);
                prgsBarLogin.setVisibility(View.GONE);

            }
            @Override
            public void isError() {
                btnLogin.setVisibility(View.VISIBLE);
                prgsBarLogin.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Hata Oluştu.", Toast.LENGTH_SHORT).show();

            }
            @Override
            public void isNotMail() {
                btnLogin.setVisibility(View.VISIBLE);
                prgsBarLogin.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Böyle Bir E-posta Kullanılmamakta.", Toast.LENGTH_SHORT).show();

            }
            @Override
            public void isFalsePassword() {
                btnLogin.setVisibility(View.VISIBLE);
                prgsBarLogin.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Yalnış Şifre", Toast.LENGTH_SHORT).show();

            }
        });

    }
    private void register(User user) {

        btnRegister.setVisibility(View.INVISIBLE);
        prgsBarRegister.setVisibility(View.VISIBLE);
        UserManager manager=new UserManager();
        manager.addUser(user);
        manager.registerResponse(new UserManager.ListenerRegister() {
            @Override
            public void isSuccessful() {
                btnRegister.setVisibility(View.VISIBLE);
                prgsBarRegister.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Kayıt Başarılı.", Toast.LENGTH_SHORT).show();
                showView();
                setNull();
            }
            @Override
            public void isSameMail() {
                btnRegister.setVisibility(View.VISIBLE);
                prgsBarRegister.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "E-Posta Kullanılmakta.", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void isError() {
                btnRegister.setVisibility(View.VISIBLE);
                prgsBarRegister.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Hata oluştu.", Toast.LENGTH_SHORT).show();
            }
        });



    }
    private User setUser() {
        User user=new User(LoginActivity.this);
        if(viewLogin.getVisibility()==View.VISIBLE){
            EditText edtMail=findViewById(R.id.edtMail);
            EditText edtPassword=findViewById(R.id.edtPassword);
            user.setMail(edtMail.getText().toString());
            user.setPassword(edtPassword.getText().toString());
            return user;
        }else{
            EditText edtNameSurname=viewRegister.findViewById(R.id.edtNameSurname);
            EditText edtMail=viewRegister.findViewById(R.id.edtMail);
            EditText edtCompanyName=viewRegister.findViewById(R.id.edtCompanyName);
            EditText edtPassword=viewRegister.findViewById(R.id.edtPassword);
            EditText edtPassword2=viewRegister.findViewById(R.id.edtPassword2);

            user.setNameSurname(edtNameSurname.getText().toString());
            user.setMail(edtMail.getText().toString());
            user.setCompanyName(edtCompanyName.getText().toString());
            user.setPassword(edtPassword.getText().toString());
            user.setPassword2(edtPassword2.getText().toString());
            return user;
        }


    }
    private boolean isValidation(User user) {

        if(viewLogin.getVisibility()==View.VISIBLE){
            return user.isLogin();
        }else{
            return user.isRegister();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cardLogin:
                User user=setUser();
                if(isValidation(user)){
                    login(user);
                }
                break;
            case R.id.txtRegister:
                showView();
                break;
            case R.id.txtLogin:
                showView();
                break;
            case R.id.cardRegister:
                User user1=setUser();
                if(isValidation(user1)){
                    register(user1);
                }
                break;
            default:
        }

    }
    @Override
    public void onBackPressed() {
       if(viewRegister.getVisibility()==View.VISIBLE){
           showView();
       }else{
           finish();
       }
    }
}
