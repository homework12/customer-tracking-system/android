package com.ekmekk.yazilimbakimi.screen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.R;

public class PhotoAddDialog extends Dialog {

    private View.OnClickListener yes=null;
    private View.OnClickListener no=null;
    private Bitmap bitmap=null;

    public PhotoAddDialog(@NonNull Context context) {
        super(context);
    }

    public PhotoAddDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected PhotoAddDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_gallery_photo_add);
        initDialog();
    }

    private void initDialog() {

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.windowAnimations = R.style.DialogAnimation;
        getWindow().setAttributes(lp);
        getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        ImageView imgPhoto=findViewById(R.id.imageView14);
        ImageView imgCancel=findViewById(R.id.imgCancel);
        ImageView imgOK=findViewById(R.id.imgOk);

        imgOK.setOnClickListener(yes);
        imgCancel.setOnClickListener(no);

        imgPhoto.setImageBitmap(getBitmap());


        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    public void setYes(View.OnClickListener yes) {
        this.yes = yes;

    }

    public void setNo(View.OnClickListener no) {
        this.no = no;
        dismiss();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
