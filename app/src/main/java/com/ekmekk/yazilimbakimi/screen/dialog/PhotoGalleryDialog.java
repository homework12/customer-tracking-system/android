package com.ekmekk.yazilimbakimi.screen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.R;

public class PhotoGalleryDialog extends Dialog {


    private View.OnClickListener gallery=null;
    private View.OnClickListener camera=null;
    private View.OnClickListener cancel=null;
    private Dialog dialog;


    public PhotoGalleryDialog(@NonNull Context context) {
        super(context);
    }

    public PhotoGalleryDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected PhotoGalleryDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_gallery_photo);
        initDialog();
    }

    private void initDialog() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.FILL_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        getWindow().setAttributes(lp);
        getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ConstraintLayout constCamera=findViewById(R.id.constCamera);
        ConstraintLayout constGallery=findViewById(R.id.constGallery);
        ConstraintLayout constCancel=findViewById(R.id.constCancel);


        constGallery.setOnClickListener(gallery);
        constCamera.setOnClickListener(camera);
        constCancel.setOnClickListener(cancel);


        constCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });



    }


    public void setGallery(View.OnClickListener gallery) {
        this.gallery = gallery;
    }

    public void setCamera(View.OnClickListener camera) {
        this.camera = camera;
    }

    public void setCancel(View.OnClickListener cancel) {
        dismiss();
        this.cancel = cancel;
    }

}
