package com.ekmekk.yazilimbakimi.screen.stock;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.helper.Constant;
import com.ekmekk.yazilimbakimi.helper.Tolbar;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        init();
        Tolbar include=new Tolbar(ProductActivity.this,"Ürün Bilgisi");
        include.create();
        urunBIlgisi();
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButlerRequest request = Butler.Instance().Path("product").DELETE(Account.getInstance().getProductID());
                request.SEND(new IButlerListener() {
                    @Override
                    public void OnResponse(String data) {
                        Toast.makeText(ProductActivity.this, "Ürün silindi.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });

    }


    private void init() {
        RoundedImageView imgProduct=findViewById(R.id.imgProduct);
        Glide.with(this).load("https://img-s2.onedio.com/id-535a5930df0ca4ec5485e6bd/rev-0/raw/s-f1d5e6dc6ad905e27e5c42ae6625486c081165bc.jpg").into(imgProduct);
    }

    private void urunBIlgisi() {
        ButlerRequest request = Butler.Instance().Path("product").GET(Account.getInstance().getProductID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                TextView txtCihazIsmi=findViewById(R.id.textView58);
                TextView txtTArih=findViewById(R.id.textView61);
                TextView txtAciklama=findViewById(R.id.textView72);
                TextView txtKategori=findViewById(R.id.textView62);
                TextView txtUrunAdeti=findViewById(R.id.textView64);
                TextView txtAlıs=findViewById(R.id.textView66);
                TextView txtSatis=findViewById(R.id.textView68);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    JSONObject array=jsonObject.getJSONObject("product");
                    txtCihazIsmi.setText(array.getString("productName"));
                    txtTArih.setText(array.getString("creationDate"));
                    txtAciklama.setText(array.getString("productExplanation"));
                    txtKategori.setText(array.getString("categoryName"));
                    txtUrunAdeti.setText(array.getString("productCount"));
                    txtAlıs.setText(array.getString("buyingPrice"));
                    txtSatis.setText(array.getString("sellingPrice"));
                    RoundedImageView imgProduct=findViewById(R.id.imgProduct);
                    Glide.with(getApplicationContext()).load(Constant.SERVER_URL_IMAGE_PATH+array.getString("productImage")).into(imgProduct);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}

// TODO: 19.10.2020 Ürünün satıldığı müşteriler
// TODO: 19.10.2020 Stoğa eklenen ürünün tarihi

