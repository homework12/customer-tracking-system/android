package com.ekmekk.yazilimbakimi.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.CDate;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;
import com.furkanbahadirozdemir.fountain.Fountain;
import com.furkanbahadirozdemir.fountain.Interface.IRecyclerView;
import com.furkanbahadirozdemir.fountain.List.RecyclerView.FountainRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InstallmentActivity extends AppCompatActivity {
    FountainRecyclerView vieww;
    private ArrayList<CDate> arrayListTaksit=new ArrayList<>();
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installment);
        recyclerView=findViewById(R.id.rvTaksit);
        initInclude();
        taksit();
        getPaymentInformation();
    }

    private void getPaymentInformation() {
        ButlerRequest request = Butler.Instance().Path("customer/payment").GET(Account.getInstance().getCustomerID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("taksit sayfası",data);


                try {
                    JSONArray array=new JSONArray(data);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object1=array.getJSONObject(i);
                        String[] date=object1.getString("paymentDate").split("/");
                        date[1]=getAy(Integer.parseInt(date[1]));
                        CDate cDate=new CDate();
                        cDate.id=object1.getString("paymentID");
                        cDate.ay=date[1];
                        cDate.gun=date[0];
                        cDate.yil=date[2];

                        cDate.ok=object1.getString("paymentStatus");
                        cDate.taksitTL=object1.getString("installmentAmount")+" Tl";
                        cDate.counter=i+1;
                        arrayListTaksit.add(cDate);


                    }
                    vieww.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void initInclude() {
        View view=findViewById(R.id.includeTopMenu);
        ImageView imgBack=view.findViewById(R.id.imgBack);
        TextView txtActivityName=view.findViewById(R.id.txtActivityName);
        txtActivityName.setText("Alınacak Ücretler");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void taksit() {
        vieww = Fountain.Initialize(InstallmentActivity.this).List().RecyclerView();
        vieww.SetContext(InstallmentActivity.this);
        vieww.SetItemList(arrayListTaksit);
        vieww.SetLayout(R.layout.item_financial_installment);
        final int[] counter = {1};
        vieww.SetListener(new IRecyclerView() {
            @Override
            public void OnView(View view, Object obj) {
                final CDate mClass=(CDate) obj;

                final TextView txtCounter=view.findViewById(R.id.textView58);
                final TextView txtTaksit=view.findViewById(R.id.textView95);

                TextView txtGun=view.findViewById(R.id.textView63);
                TextView txtAy=view.findViewById(R.id.textView64);
                TextView txtYil=view.findViewById(R.id.textView65);
                TextView txtTaksitMiktari=view.findViewById(R.id.textView66);
                TextView txtTarih=view.findViewById(R.id.textView67);
                final Button button=view.findViewById(R.id.button17);
                txtCounter.setText(""+ mClass.counter);

                txtGun.setText(mClass.gun);
                txtAy.setText(mClass.ay);
                txtYil.setText(mClass.yil);
                txtTaksitMiktari.setText(mClass.taksitTL);

                counter[0] = counter[0] +1;

                if(mClass.ok.equals("1")){
                    txtCounter.setTextColor(Color.GREEN);
                    txtTaksit.setTextColor(Color.GREEN);
                    button.setText("Ödemeyi İptal Et");
                    txtTarih.setText("Ödendi");
                }else {
                    txtCounter.setTextColor(Color.RED);
                    txtTaksit.setTextColor(Color.RED);
                    button.setText("Ödeme Yap");
                    txtTarih.setText("Ödenmedi");
                }

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        ButlerRequest request = Butler.Instance().Path("customer/payDebt/installment").POST();
                        request.Params("paymentID",mClass.id);
                        request.SEND(new IButlerListener() {
                            @Override
                            public void OnResponse(String data) {
                                arrayListTaksit.clear();
                                getPaymentInformation();
                                vieww.notifyDataSetChanged();
                            }
                        });



                    }
                });




            }


            @Override
            public void OnViewHolder(View view) {

            }
        });
        vieww.Create(recyclerView);
        RecyclerView.LayoutManager mLayoutmanager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutmanager);

    }

    private String getAy(int id){
        String[] aylar={"Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"};
        return aylar[id-1];
    }

}