package com.ekmekk.yazilimbakimi.screen.customer.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.adapter.ViewPagerAdapter;
import com.ekmekk.yazilimbakimi.databasemanager.CustomerManager;
import com.ekmekk.yazilimbakimi.model.Customer;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.add.InformationFragment;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.add.MachineInformationFragment;
import com.ekmekk.yazilimbakimi.screen.customer.fragment.add.PaymentInformationFragment;
import com.google.android.material.tabs.TabLayout;

public class CustomerAddActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add);
        init();
        setupViewPager();

    }


    private void init() {
        int[] id =new int[]{R.id.imgBack, R.id.constAddUser};
        for(int i : id ){
            findViewById(i).setOnClickListener(this);
        }

    }

    private void setupViewPager() {

        ViewPager viewPager =  findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new InformationFragment(), "İletişim Bilgileri");
        adapter.addFrag(new MachineInformationFragment(), "Makine Bilgileri");
        adapter.addFrag(new PaymentInformationFragment(), "Ödeme Bilgileri");
        viewPager.setAdapter(adapter);
        //Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins_bold);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
            case R.id.constAddUser:

                Customer customer=new Customer();
                customer.setInformation(InformationFragment.Instance.information());
                customer.setMachine(MachineInformationFragment.Instance.machine());
                customer.setPayment(PaymentInformationFragment.Instance.payment());

                CustomerManager manager=new CustomerManager();
                manager.CustomerAdd(customer);
                manager.setOnClickListener(new CustomerManager.Response() {
                    @Override
                    public void Successful(Object o) {
                        Toast.makeText(CustomerAddActivity.this, "Müşteri eklendi.", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void Error(String errorType) {

                    }
                });
                break;
            default:
        }
    }
}
