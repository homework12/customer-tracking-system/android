package com.ekmekk.yazilimbakimi.screen.customer.activity;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ekmekk.yazilimbakimi.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.List;

public class AddAddressMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address_map);
        initInclude();
        initMap();
    }



    @SuppressLint("SetTextI18n")
    private void initInclude() {
        View view=findViewById(R.id.include);
        ImageView img=view.findViewById(R.id.imgBack);
        TextView txtTitle=view.findViewById(R.id.txtActivityName);
        txtTitle.setText("Adres Ekleme");
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void initMap() {
        mapFragment = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                Log.d("Map","Map clicked");

                double latitude = point.latitude;
                double longitude = point.longitude;


                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(point);
                markerOptions.title("Adres Konumu");
                googleMap.clear();
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(point));
                googleMap.addMarker(markerOptions);
                Geocoder gCoder = new Geocoder(AddAddressMapActivity.this);
                List<Address> addresses;
                try {
                    addresses = gCoder.getFromLocation(latitude, longitude, 1);

                    if (addresses != null && addresses.size() > 0) {
                        StringBuilder stringBuilder = new StringBuilder();

                        if (addresses.get(0).getThoroughfare() != null) {
                            stringBuilder.append(addresses.get(0).getThoroughfare());
                            stringBuilder.append(", ");
                        }

                        if (addresses.get(0).getSubLocality() != null) {
                            stringBuilder.append(addresses.get(0).getSubLocality());
                            stringBuilder.append(", ");
                        }

                        if (addresses.get(0).getAdminArea() != null) {
                            stringBuilder.append(addresses.get(0).getAdminArea());
                        }


                    }
                } catch (IOException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                googleMap.setOnMyLocationChangeListener(null);


            }
        });

    }
}
