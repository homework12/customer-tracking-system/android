package com.ekmekk.yazilimbakimi.screen.customer.fragment.information;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.screen.InstallmentActivity;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerInformationActivity;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PaymentInformationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match


    public PaymentInformationFragment() {
        // Required empty public constructor
    }


    Button button;
    boolean isNakit=false;
    String isUcreti,alinanUcret,kalanUcret;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_payment_information, container, false);
        button=view.findViewById(R.id.button834);
        getPaymentInformation(view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNakit){
                    odemeYap();
                }else {
                    startActivity(new Intent(getContext(), InstallmentActivity.class));
                }

            }
        });
        return view;
    }

    private void odemeYap() {
        final Dialog dialog=new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_add_payment);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogInOut;
        TextView btnExit=dialog.findViewById(R.id.Popup_CustomerUpdateFinancial_TextView_Exit);
        TextView btnOdemeYap=dialog.findViewById(R.id.Popup_CustomerUpdateFinancial_Textview_Edit);
        TextView txtIsucreti,txtAlinanUret,txtKalanUcret;
        txtIsucreti=dialog.findViewById(R.id.textView170);
        txtAlinanUret=dialog.findViewById(R.id.textView171);
        txtKalanUcret=dialog.findViewById(R.id.Popup_CustomerUpdateFinancial_EdittText_OdenecekUcret);

        txtIsucreti.setText(isUcreti);
        txtAlinanUret.setText(alinanUcret);
        txtKalanUcret.setText(kalanUcret);
        final EditText edtPayment=dialog.findViewById(R.id.edtAddPayment);

        btnOdemeYap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButlerRequest request = Butler.Instance().Path("customer/payDebt/cash").POST();
                request.Params("customerID",Account.getInstance().getCustomerID());
                request.Params("amountPaid",edtPayment.getText().toString());
                request.SEND(new IButlerListener() {
                    @Override
                    public void OnResponse(String data) {
                        Toast.makeText(getContext(), "Ödeme Başarılı bir şekilde gerçekleştirildi.", Toast.LENGTH_SHORT).show();
                        getPaymentInformation(view);
                        dialog.dismiss();
                    }
                });

            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }



    private void getPaymentInformation(final View view) {
        ButlerRequest request = Butler.Instance().Path("customer/payment").GET(Account.getInstance().getCustomerID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("ücretBilgileri",data);
                TextView txtYapilanis=view.findViewById(R.id.textView75);
                TextView txtOdenenUcret=view.findViewById(R.id.textView76);
                TextView txtKalanUcret=view.findViewById(R.id.textView77);


                try {
                    JSONArray array=new JSONArray(data);
                    JSONObject object=array.getJSONObject(0);
                    isUcreti=object.getString("orderCost");
                    txtYapilanis.setText(object.getString("orderCost"));
                    if(object.getString("type").equals("0")){
                        button.setText("Ödeme Yap");
                        isNakit=true;
                    }else if(object.getString("type").equals("1")){
                        button.setText("Taksitler");
                        isNakit=false;
                    }
                    if(array.length()==1){
                        txtOdenenUcret.setText(object.getString("amountPaid"));
                        txtKalanUcret.setText(object.getInt("orderCost")-object.getInt("amountPaid")+"");
                        txtYapilanis.setText(object.getString("orderCost"));
                        alinanUcret=object.getString("amountPaid");
                        kalanUcret=object.getInt("orderCost")-object.getInt("amountPaid")+"";
                    }else{
                        int toplamborc=0;
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1=array.getJSONObject(i);
                            if(object1.getString("paymentStatus").equals("0")){
                                toplamborc+=object1.getInt("installmentAmount");
                            }
                        }
                        alinanUcret=""+toplamborc;
                        kalanUcret=(object.getInt("orderCost")-toplamborc)+"";
                        txtKalanUcret.setText(""+(object.getInt("orderCost")-(object.getInt("orderCost")-toplamborc)));
                        txtOdenenUcret.setText((object.getInt("orderCost")-toplamborc)+"");


                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
}
