package com.ekmekk.yazilimbakimi.screen.customer.fragment.information;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MachineInformationFragment extends Fragment {


    public MachineInformationFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_machine_information, container, false);
        getMachineInformation(view);
        return view;
    }

    private void getMachineInformation(final View view) {
        ButlerRequest request = Butler.Instance().Path("customer/machine").GET(Account.getInstance().getCustomerID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                Log.e("makinebilgiler",data);
                TextView txtMontajTarihi=view.findViewById(R.id.textView75);
                TextView txtCihazModeli=view.findViewById(R.id.textView76);
                TextView txtYapilanİslem=view.findViewById(R.id.textView77);
                TextView txtBakimaKalanGun=view.findViewById(R.id.textView78);

                try {
                    JSONArray array=new JSONArray(data);
                    JSONObject object=array.getJSONObject(0);
                    txtMontajTarihi.setText(object.getString("assemblyDate"));
                    txtCihazModeli.setText(object.getString("machineName"));
                    txtBakimaKalanGun.setText(object.getString("repairDate"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    private void init(View view) {

    }
}
