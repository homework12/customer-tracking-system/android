package com.ekmekk.yazilimbakimi.helper;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ekmekk.yazilimbakimi.R;

public class Tolbar {

    private String title=null;
    private Activity mContext;
    private boolean onBackPressed=true;

    public Tolbar(Context context, String title) {
        this.mContext=getActivity(context);
        setTitle(title);
    }

    public Tolbar(Activity mContext) {
        this.mContext = mContext;
    }

    public void create(){
        View view=mContext.findViewById(R.id.includeTopMenu);
        TextView txtActivityName=view.findViewById(R.id.txtActivityName);
        txtActivityName.setText(getTitle());
        if(isOnBackPressed()){
            ImageView imgBack=view.findViewById(R.id.imgBack);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.finish();

                }
            });
        }


    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOnBackPressed() {
        return onBackPressed;
    }

    public void setOnBackPressed(boolean onBackPressed) {
        this.onBackPressed = onBackPressed;
    }

    private Activity getActivity(Context context) {
        if (context == null) {
            return null;
        }
        if (context instanceof Activity){
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return getActivity(((ContextWrapper)context).getBaseContext());
        }
        return null;
    }


}
