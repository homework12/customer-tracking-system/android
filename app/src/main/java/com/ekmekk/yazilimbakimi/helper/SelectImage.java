package com.ekmekk.yazilimbakimi.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ekmekk.yazilimbakimi.R;


public class SelectImage extends Activity {



    private Activity mContext;
    private Dialog dialog;
    private ISelectImage listener;



    public SelectImage(Activity activity) {
        this.mContext=activity;
        initDialog();
    }


    private void initDialog(){
        dialog = new Dialog(mContext, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_select_gallery_photo);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        ConstraintLayout constCamera=dialog.findViewById(R.id.constCamera);
        ConstraintLayout constGallery=dialog.findViewById(R.id.constGallery);
        ConstraintLayout constCancel=dialog.findViewById(R.id.constCancel);

        constGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("asdasdasd","asdasd");

                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                mContext.startActivityForResult(pickPhoto , Constant.GALLERY);
            }
        });

        constCamera.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Log.e("asdasdasd","asdasd");


                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePicture.resolveActivity(mContext.getPackageManager()) != null) {
                    mContext.startActivityForResult(takePicture, Constant.CAMERA);
                }
            }
        });

        constCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    public void setListner(ISelectImage listener){
        this.listener=listener;
    }



    public interface ISelectImage { // create an interface
        void SelectImage(Bitmap bitmap); // create callback function,

    }

}
