package com.ekmekk.yazilimbakimi.helper;

import android.content.Context;
import android.util.Log;


import com.ekmekk.yazilimbakimi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SearchPlaces {

    public static ArrayList<String> getIL(Context context){
        String iller= context.getString(R.string.iller);;
        ArrayList<String> list=new ArrayList<>();
        try {
            JSONArray array=new JSONArray(iller);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object=array.getJSONObject(i);
                list.add(object.getString("sehir_title"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
    public static ArrayList<String> getIlce(Context context, String ilce){
        String ilID=getIlID(context,ilce);
        ArrayList<String> list=new ArrayList<>();
        BufferedReader reader = null;
        String ilceKey="-1";
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("ilce.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                JSONObject object=new JSONObject(mLine);
                if(object.getString("ilce_sehirkey").equals(ilID)){
                    list.add(object.getString("ilce_title"));

                }

            }


        } catch (IOException e) {
            //log the exception
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }


        return list;
    }
    public static ArrayList<String> getMahalle(Context context, String ilce){
        ArrayList<String> list=new ArrayList<>();
        String ilceKey=getIlceID(context,ilce);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("mahalle.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                JSONObject object=new JSONObject(mLine);
                Log.e("hata",object.getString("id"));
                if(object.getString("id").equals(ilceKey)){
                    list.add(object.getString("ad"));
                }


            }
        } catch (IOException e) {
            //log the exception
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }


        return list;
    }

    private static String getIlID(Context context,String s){
        String iller= context.getString(R.string.iller);;
        ArrayList<String> list=new ArrayList<>();
        try {
            JSONArray array=new JSONArray(iller);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object=array.getJSONObject(i);
                if(s.equals(object.getString("sehir_title"))){
                    return object.getString("sehir_key");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "-1";

    }
    private static String getIlceID(Context context,String s){
        ArrayList<String> list=new ArrayList<>();
        BufferedReader reader = null;
        String ilceKey="-1";
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("ilce.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                JSONObject object=new JSONObject(mLine);
                if(object.getString("ilce_title").equals(s)){
                   return object.getString("ilce_key");

                }

            }


        } catch (IOException e) {
            //log the exception
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }


        return "-1";

    }
}
