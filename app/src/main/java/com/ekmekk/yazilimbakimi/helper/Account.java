package com.ekmekk.yazilimbakimi.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Account {

    private Activity appContext;
    private static Account instance;

    public Account() {}

    public static synchronized  Account getInstance(){
        return instance == null ?
                (instance = new Account()):
                instance;
    }

    public static Context get(){
        return getInstance().getContext();
    }

    private Context getContext(){
        return appContext;
    }

    public void init(Activity context){
        if(appContext == null){
            appContext = context;
        }
    }

    public void setUserID(String id){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constant.USER_ID, id);
        editor.apply();
    }

    public boolean isUserLogin(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        String userID = sharedPref.getString(Constant.USER_ID, null);
        return userID==null ? false : true ;
    }

    public String getUserID(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        String userID = sharedPref.getString(Constant.USER_ID, null);
        return userID == null ? "-1" : userID;

    }
    public String getCustomerID(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        String userID = sharedPref.getString(Constant.CUSTOMER_ID, null);
        return userID == null ? "-1" : userID;

    }
    public void setCustomerID(String id){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constant.CUSTOMER_ID, id);
        editor.apply();
    }

    public String getStockID(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        String userID = sharedPref.getString( "stockID", null);
        return userID == null ? "-1" : userID;

    }

    public void setCategoryID(String id){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("stockID", id);
        editor.apply();
    }


    public String getProductID(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        String userID = sharedPref.getString( "sproductID", null);
        return userID == null ? "-1" : userID;

    }
    public void setProductID(String id){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("sproductID", id);
        editor.apply();
    }

    public void userLogout(){
        SharedPreferences sharedPref = appContext.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constant.USER_ID, null);
        editor.apply();
    }

}
