package com.ekmekk.yazilimbakimi.helper;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Base64;
import android.view.View;

import com.ekmekk.yazilimbakimi.screen.dialog.PhotoAddDialog;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class OnActivityResult {

    private Context mContext;
    private PhotoAddDialog dialog;
    private response listener;

    public OnActivityResult(Context mContext) {
        this.mContext = mContext;

    }

    public void setImageOk(response listener){
        this.listener=listener;
    }

    private void initDialog(final Bitmap bitmap) {
        dialog=new PhotoAddDialog(mContext);
        dialog.setBitmap(bitmap);
        dialog.setYes(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.ok(convert(bitmap),bitmap);
            }
        });
        dialog.show();
    }

    public void setData(int requestCode, int resultCode, Intent data){
        if (resultCode != RESULT_CANCELED && data!=null) {
        switch (requestCode) {
            case Constant.CAMERA:
                if (resultCode == RESULT_OK) {
                    Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                    if(selectedImage!= null) {
                        selectedImage= ImagePicker.getResizedBitmap(selectedImage, 3080);
                        initDialog(selectedImage);
                    }
                }
                break;
            case Constant.GALLERY:
                if (resultCode == RESULT_OK) {
                    Bitmap selectedImage = ImagePicker.getImageFromResult(mContext, resultCode, data);
                    if(selectedImage!=null){
                        initDialog(selectedImage);
                    }
                }
                break;
            default:
        }
        }
    }

    public interface response{
        void ok(String imgString,Bitmap bitmap);
    }

    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

}
