package com.ekmekk.yazilimbakimi.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ekmekk.yazilimbakimi.R;


public class Alert extends Dialog {

    private String title;
    private String message;
    private View.OnClickListener btnYesListener=null;
    private View.OnClickListener btnNoListener=null;
    private String btnPositiveTitle="Evet";
    private String btnNegativeTitle="Hayır";

    public Alert(@NonNull Context context) {
        super(context);
    }

    public Alert(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected Alert(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_notification);
        initDialog();

    }

    private void initDialog() {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogInOut;
        getWindow().setAttributes(lp);
        TextView txtTitle=findViewById(R.id.txtTitle);
        TextView txtMessage=findViewById(R.id.txtMesaj);
        Button btnYes=findViewById(R.id.btnYes);
        Button btnNo=findViewById(R.id.btnNo);
        txtTitle.setText(getTitle());
        txtMessage.setText(getMessage());
        btnYes.setText(btnPositiveTitle);
        btnNo.setText(btnNegativeTitle);
        btnYes.setOnClickListener(btnYesListener);
        btnNo.setOnClickListener(btnNoListener);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPositveButton(View.OnClickListener onClickListener) {
        dismiss();
        this.btnYesListener = onClickListener;
    }

    public void setNegativeButton( View.OnClickListener onClickListener) {
        dismiss();
        this.btnNoListener = onClickListener;


    }

    public void setPositveButton(String title ,View.OnClickListener onClickListener) {
        dismiss();
        this.btnPositiveTitle=title;
        this.btnYesListener = onClickListener;
    }

    public void setNegativeTitle(String title) {
        this.btnNegativeTitle=title;
    }
}
