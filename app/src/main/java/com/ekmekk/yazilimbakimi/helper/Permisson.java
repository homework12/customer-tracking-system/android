package com.ekmekk.yazilimbakimi.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Permisson  {


    public static boolean isCamera(Context context){
        boolean isGranted=ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(context,  Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(context,  Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED;
        if (isGranted) {
            ActivityCompat
                    .requestPermissions(
                            (Activity) context,
                            new String[] {
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                            },
                            0);
        }

        return !isGranted;
    }
    public static boolean isLocation(Context context){
        boolean isGranted=ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(context,  Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED;
        if (isGranted) {
            ActivityCompat
                    .requestPermissions(
                            (Activity) context,
                            new String[] {
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                            },
                            1);

            return false;
        }

        LocationService service=new LocationService();
        if(service.isGPSEnabled(context)){
         service.isGpsEnabledDialog(context);
         return false;
        }
        return !isGranted;
    }





}
