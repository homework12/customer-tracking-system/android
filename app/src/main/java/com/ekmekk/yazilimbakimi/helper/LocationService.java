package com.ekmekk.yazilimbakimi.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;


import com.ekmekk.yazilimbakimi.model.Address;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

public class LocationService implements LocationListener {

    private  Context mContext;
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private Location konum;
    private double enlem; // latitude
    private double boylam; // longitude

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    private LocationManager locationManager;

    public LocationService(Context context) {
        this.mContext = context;
        getLocation();
    }

    public LocationService( ) {

    }

    @SuppressLint("MissingPermission")
    private Location getLocation() {
        try {

            if (isGPSEnabled(mContext)) {
                isGpsEnabledDialog(mContext);
            } else {
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        konum = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (konum != null) {
                            enlem = konum.getLatitude();
                            boylam = konum.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (konum == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            konum = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            if (konum != null) {
                                enlem = konum.getLatitude();
                                boylam = konum.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return konum;
    }

    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(LocationService.this);
        }
    }
    public double getEnlem(){
        if(konum != null){
            enlem = konum.getLatitude();
        }
        return enlem;
    }
    public double getBoylam(){
        if(konum != null){
            boylam = konum.getLongitude();
        }
        return boylam;
    }






    public void isGpsEnabledDialog(final Context context){
        final Alert alert=new Alert(context);
        alert.setTitle("GPS İzni");
        alert.setMessage("Gps kapalı, mevcut konumu almak için gps açınız.");
        alert.setNegativeTitle("İptal");
        alert.setPositveButton("Aç",new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
                context.startActivity(new Intent(action));
            }
        });
        alert.show();
    }
    public boolean isGPSEnabled(Context context){
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return !isGPSEnabled && !isNetworkEnabled;
    }
    public Address getAddress() {

        Address mAddres=new Address();
        if(isGPSEnabled(mContext)){
            return null;
        }
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<android.location.Address> addresses = geocoder.getFromLocation(enlem, boylam, 1);
            mAddres.setLatitude(String.valueOf(enlem));
            mAddres.setLongitude(String.valueOf(boylam));
            if (addresses != null && addresses.size() > 0) {
                android.location.Address address = addresses.get(0);
                if(addresses.get(0).getAdminArea()!=null){
                    mAddres.setProvince(addresses.get(0).getAdminArea());
                }
                if(addresses.get(0).getSubAdminArea()!=null){
                    mAddres.setDistrict(addresses.get(0).getSubAdminArea());
                }
                if(addresses.get(0).getSubLocality()!=null){
                    mAddres.setNeighborhood(addresses.get(0).getSubLocality());
                }


            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return mAddres;
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
