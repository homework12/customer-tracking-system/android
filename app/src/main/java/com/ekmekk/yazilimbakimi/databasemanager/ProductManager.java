package com.ekmekk.yazilimbakimi.databasemanager;

import android.util.Log;

import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductManager {

    private Listener listener;
    private ArrayList<Stok> arrayList=new ArrayList<>();


    public void addProduct(Stok stok){
        ButlerRequest request = Butler.Instance().Path("product").POST();
        request.Params("productImage", stok.getProductImage());
        request.Params("productExplanation", stok.getProductExplanation());
        request.Params("categoryID", stok.getCategoryID());
        request.Params("productCount", stok.getProductCount());
        request.Params("productName", stok.getProductName());
        request.Params("buyingPrice", stok.getBuyingPrice());
        request.Params("sellingPrice", stok.getSellingPrice());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    if(object.getString("response").equals("1")){
                        listener.Successful(true);
                    }else if(object.getString("response").equals("0")){
                        listener.Error();
                    }else {
                        listener.Error();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void getProductList(){
        ButlerRequest request = Butler.Instance().Path("stock").GET(Account.getInstance().getUserID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    Log.e("stockList",data);
                    if(object.getString("response").equals("1")){
                        JSONArray array=object.getJSONArray("list");
                        for (int i = 0; i < array.length(); i++) {
                            Stok stok=new Stok();
                            JSONObject object1=array.getJSONObject(i);
                            stok.setCategoryID(object1.getString("categoryID"));
                            stok.setCategoryName(object1.getString("categoryName"));
                            stok.setStockProductCount(object1.getString("stockProductCount"));
                            stok.setCreationDate(object1.getString("creationDate"));
                            arrayList.add(stok);
                        }
                        listener.Successful(arrayList);
                    }else if(object.getString("response").equals("0")){
                        listener.Error();
                    }else {
                        listener.Error();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void updateProduct(Stok stok){
        ButlerRequest request = Butler.Instance().Path("stock").PUT();
        request.Params("categoryID", stok.getCategoryID());
        request.Params("categoryName", stok.getCategoryName());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    if(object.getString("response").equals("1")){
                        listener.Successful(true);
                    }else if(object.getString("response").equals("0")){
                        listener.Error();
                    }else {
                        listener.Error();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void deleteProduct(Stok stok){
        ButlerRequest request = Butler.Instance().Path("stock").DELETE(stok.getCategoryID());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    if(object.getString("response").equals("1")){
                        listener.Successful(true);
                    }else if(object.getString("response").equals("0")){
                        listener.Error();
                    }else {
                        listener.Error();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void  setOnClickListener(Listener listener){
        this.listener=listener;
    }
    public interface Listener{
        void Successful(Object object);
        void Error();
    }
}
