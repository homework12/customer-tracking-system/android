package com.ekmekk.yazilimbakimi.databasemanager;

import android.util.Log;

import com.ekmekk.yazilimbakimi.model.Customer;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomerManager {

    public Response listener;



    public void CustomerAdd(Customer customer){
        ButlerRequest request = Butler.Instance().Path("customer").POST();
        request.Params("customerFullname",customer.getInformation().getName());
        request.Params("customerCellphone",customer.getInformation().getPhone());
        request.Params("customerHomephone",customer.getInformation().getHousePhone());
        request.Params("city",customer.getInformation().getProvince());
        request.Params("district",customer.getInformation().getDistrict());
        request.Params("neighborhood",customer.getInformation().getNeighborhood());
        request.Params("street",customer.getInformation().getStreetAvenue());
        request.Params("apartmentNo",customer.getInformation().getBuildingNumber());
        request.Params("longitude",customer.getInformation().getLongitude());
        request.Params("latitude",customer.getInformation().getLatitude());
        request.Params("machineName",customer.getMachine().getModel());
        request.Params("direction",customer.getInformation().getKisaTarif());
        request.Params("repairDate",customer.getMachine().getMaintenanceDate());
        request.Params("assemblyDate",customer.getMachine().getInstallationDate());
        request.Params("paymentType",customer.getPayment().getPaymnetType());
        request.Params("orderCost",customer.getPayment().getTotalPayment());
        request.Params("startingDate",customer.getPayment().getInstallmentDate());
        request.Params("installmentCount",customer.getPayment().getInstallmentCount());
        request.Params("amountPaid",customer.getPayment().getFromPayment());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                listener.Successful(data);
            }
        });
    }
    public void CustomerGet(String id, final String s){
        ButlerRequest request = Butler.Instance().Path("customer/info").POST();
        request.Params("userID",id);
        request.Params("type",s);
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    Log.e("ücretler",data);
                    ArrayList<CustomerInformation> arrayList=new ArrayList<>();
                    JSONArray array=new JSONArray(data);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject=array.getJSONObject(i);
                        CustomerInformation customer=new CustomerInformation();
                        customer.setInformationID(jsonObject.getString("customerID"));
                        customer.setName(jsonObject.getString("customerFullname"));
                        customer.setPhone(jsonObject.getString("customerCellphone"));
                        if(s.equals("1")){
                            customer.setDayLeft(jsonObject.getString("date")+ " Gün");

                        }else if (s.equals("2")){
                            customer.setDayLeft(jsonObject.getString("debt") +" TL");
                        }else if(s.equals("3")){
                            customer.setLatitude(jsonObject.getString("latitude"));
                            customer.setLongitude(jsonObject.getString("longitude"));
                            customer.setDayLeft(jsonObject.getString("date"));
                        }
                        customer.setInformationID(jsonObject.getString("customerID"));
                        arrayList.add(customer);
                    }
                    listener.Successful(arrayList);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }






    public void setOnClickListener(Response listener){
        this.listener=listener;
    }
    public interface Response{
        void Successful(Object o);
        void Error(String errorType);
    }
}
