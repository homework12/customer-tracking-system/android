package com.ekmekk.yazilimbakimi.databasemanager;

import android.util.Log;

import com.ekmekk.yazilimbakimi.model.User;
import com.furkanbahadirozdemir.butler.Butler;
import com.furkanbahadirozdemir.butler.ButlerRequest;
import com.furkanbahadirozdemir.butler.Interface.IButlerListener;

import org.json.JSONException;
import org.json.JSONObject;

public class UserManager {

    private ListenerRegister registerListener;
    private ListenerLogin loginListener;

    public void addUser(User user){

        ButlerRequest request = Butler.Instance().Path("register").POST();
        request.Params("fullname",user.getNameSurname());
        request.Params("mail",user.getMail());
        request.Params("password",user.getPassword());
        request.Params("companyName",user.getCompanyName());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    Log.e("nesoss",data);
                    switch (object.getString("response")){
                        case "-1":
                            registerListener.isSameMail();
                            break;
                        case "1":
                            registerListener.isSuccessful();
                            break;
                        case "-2":
                            registerListener.isError();
                            break;
                        default:
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void isLogin(User user){
        //data base hatası -2
        //-1 böyle mail yok
        //0 şifre yalnış
        //1 userID
        ButlerRequest request = Butler.Instance().Path("login").POST();
        request.Params("mail",user.getMail());
        request.Params("password",user.getPassword());
        request.SEND(new IButlerListener() {
            @Override
            public void OnResponse(String data) {
                try {
                    Log.e("sdasd",data);
                    JSONObject object=new JSONObject(data);
                    switch (object.getString("response")){
                        case "0":
                            loginListener.isFalsePassword();
                            break;
                        case "1":
                            loginListener.isSuccessful(object.getString("userID"));
                            break;
                        case "-1":
                            loginListener.isNotMail();
                            break;
                        case "-2":
                            loginListener.isError();
                            break;
                        default:
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void registerResponse(ListenerRegister listener){
        this.registerListener=listener;
    }
    public void loginResponse(ListenerLogin listener){
        this.loginListener=listener;
    }


    public interface ListenerRegister{
        void isSuccessful();
        void isSameMail();
        void isError();
    }
    public interface ListenerLogin{
        void isSuccessful(String userID);
        void isError();
        void isNotMail();
        void isFalsePassword();
    }


}
