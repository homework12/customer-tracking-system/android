package com.ekmekk.yazilimbakimi.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.CustomerInformation;
import com.ekmekk.yazilimbakimi.screen.customer.activity.CustomerInformationActivity;

import java.util.ArrayList;


public class CustomerAdapter  extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {




    private Context context;
    private ArrayList<CustomerInformation> itemList ;



    public CustomerAdapter(Context context, ArrayList<CustomerInformation> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_customer_list, parent, false);
        return new ViewHolder(view);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CustomerInformation customer=itemList.get(position);

        holder.txtName.setText(customer.getName());
        holder.txtPhone.setText(customer.getPhone());
        holder.txtCount.setText(""+(position+1));
        holder.txtDayLeft.setText(customer.getDayLeft());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Account.getInstance().setCustomerID(customer.getInformationID());
                context.startActivity(new Intent(context, CustomerInformationActivity.class));
            }
        });







    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void filterList(ArrayList<CustomerInformation> filteredList) {
        itemList=filteredList;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtName,txtPhone,txtCount,txtDayLeft;
        CardView cardView;


        ViewHolder(View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtPhone=itemView.findViewById(R.id.txtPhone);
            txtCount=itemView.findViewById(R.id.txtCount);
            txtDayLeft=itemView.findViewById(R.id.txtDayLeft);
            cardView=itemView.findViewById(R.id.cardViewCustomerList);


        }
    }
}
