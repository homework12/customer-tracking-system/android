package com.ekmekk.yazilimbakimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.ekmekk.yazilimbakimi.screen.stock.ProductActivity;

import java.util.ArrayList;

public class StokListAdapter extends RecyclerView.Adapter<StokListAdapter.ViewHolder> {


    private Context mContext;
    private ArrayList<Stok> arrayList;
    public StokListAdapter(Context context,ArrayList<Stok> arrayList){
        this.mContext=context;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_stok, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Stok stok=arrayList.get(position);
        holder.txtCount.setText(String.valueOf(position+1));
        holder.txtProductName.setText(stok.getProductName());
        holder.txtProductCount.setText(stok.getStockProductCount());
        holder.cardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Account.getInstance().setProductID(stok.getProductID());
                mContext.startActivity(new Intent(mContext, ProductActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
         TextView txtCount,txtProductName, txtProductCount;
         CardView cardItem;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardItem=itemView.findViewById(R.id.cardStokCategory);
            txtCount=itemView.findViewById(R.id.txtCount);
            txtProductName=itemView.findViewById(R.id.textView56);
            txtProductCount=itemView.findViewById(R.id.textView57);
        }
    }
}
