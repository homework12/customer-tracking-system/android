package com.ekmekk.yazilimbakimi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Alert;
import com.ekmekk.yazilimbakimi.model.CustomerMachine;

import java.util.ArrayList;


public class MachinePhotosAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private static final int ITEM = 0 ;
    private static final int FOOTER=1;
    private ArrayList<CustomerMachine> arrayListPhotos;
    private LayoutInflater inflater;
    private Context context;


    private ListAdapterListener mListener;

    public MachinePhotosAdapter(Context context, ArrayList<CustomerMachine> arrayList, ListAdapterListener mListener) {
        this.context=context;
        inflater = LayoutInflater.from(context);
        this.arrayListPhotos = arrayList;
        this.mListener= mListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType==ITEM){
            View view = inflater.inflate(R.layout.rv_item_machine_photos, parent, false);
            return new MyViewHolder(view);
        }else{
            View view = inflater.inflate(R.layout.rv_item_machine_photos_add, parent, false);
            return new FooterHolder(view);
        }



    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MyViewHolder){
            final CustomerMachine photos=arrayListPhotos.get(position);
            Glide.with(context).load(photos.getUrl()).into(((MyViewHolder) holder).imgPhoto);
            ((MyViewHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.selectPhoto(photos);

                }
            });
            ((MyViewHolder) holder).cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final Alert mAlert = new Alert(context);
                    mAlert.setTitle("Fotağraf Silme");
                    mAlert.setMessage("Fotoğrafı silmek istediğinize emin misiniz?");
                    mAlert.setPositveButton( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                    mAlert.show();
                    return false;
                }
            });
        }else if(holder instanceof FooterHolder){
            ((FooterHolder) holder).cardPhotoAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.addPhoto();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arrayListPhotos.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(arrayListPhotos==null || arrayListPhotos.size()==0){
            return FOOTER;
        }else if(position<arrayListPhotos.size()){
            return ITEM;
        }else{
            return FOOTER;
        }
    }

    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgPhoto;
        CardView cardView;
        MyViewHolder(View itemView) {
            super(itemView);
            imgPhoto=itemView.findViewById(R.id.imageView9);
            cardView=itemView.findViewById(R.id.cardPhoto);

        }
        @Override
        public void onClick(View v) {

        }
    }

    private static class FooterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cardPhotoAdd;
        FooterHolder(View itemView) {
            super(itemView);
            cardPhotoAdd=itemView.findViewById(R.id.cardPhotoAdd);
        }
        @Override
        public void onClick(View v) {

        }
    }

    public interface ListAdapterListener { // create an interface
        void selectPhoto(CustomerMachine photos); // create callback function,
        void addPhoto();
    }
}

