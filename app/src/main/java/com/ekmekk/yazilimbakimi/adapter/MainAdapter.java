package com.ekmekk.yazilimbakimi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.model.M_MainCategory;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {




    private Context context;
    private ArrayList<M_MainCategory> itemList ;



    public MainAdapter(Context context, ArrayList<M_MainCategory> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_main_menu, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final M_MainCategory mainGame=itemList.get(position);

        holder.txtGameName.setText(mainGame.getName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(mainGame.getIntent());
            }
        });
        Glide.with(context).load(mainGame.getDrawable()).into(holder.imgCat);

    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtGameName;
        CardView cardView;
        ImageView imgCat;


        ViewHolder(View itemView) {
            super(itemView);
            txtGameName = itemView.findViewById(R.id.txtMain);
            cardView = itemView.findViewById(R.id.constRvItem);
            imgCat = itemView.findViewById(R.id.imgMain);

        }
    }
}