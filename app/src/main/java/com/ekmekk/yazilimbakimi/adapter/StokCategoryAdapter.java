package com.ekmekk.yazilimbakimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ekmekk.yazilimbakimi.R;
import com.ekmekk.yazilimbakimi.helper.Account;
import com.ekmekk.yazilimbakimi.model.Stok;
import com.ekmekk.yazilimbakimi.screen.dialog.CategoryInformationDialog;
import com.ekmekk.yazilimbakimi.screen.stock.StockListActivity;

import java.util.ArrayList;

public class StokCategoryAdapter extends RecyclerView.Adapter<StokCategoryAdapter.ViewHolder> {


    private Context context;
    private ArrayList<Stok> arrayList;
    private Listener listener;

    public StokCategoryAdapter(Context context, ArrayList<Stok> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_stok, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final Stok stok=arrayList.get(position);
            holder.txtCount.setText(String.valueOf(position+1));
            holder.txtCategory.setText(stok.getCategoryName());
            holder.txtProductCount.setText(stok.getStockProductCount());
            holder.cardItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Account.getInstance().setCategoryID(stok.getCategoryID());
                    context.startActivity(new Intent(context, StockListActivity.class));
                }
            });
            holder.cardItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    showDialog(stok);

                    return false;

                }
            });
    }

    private void showDialog(final Stok stok) {
        final CategoryInformationDialog dialog=new CategoryInformationDialog(context);
        dialog.setStok(stok);
        dialog.setOnClickListener(new CategoryInformationDialog.Listener() {
            @Override
            public void edit() {
                listener.edit(stok);
                dialog.dismiss();
            }
            @Override
            public void delete() {
                listener.delete(stok);
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCount,txtCategory, txtProductCount;
        CardView cardItem;
         ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardItem=itemView.findViewById(R.id.cardStokCategory);
            txtCount=itemView.findViewById(R.id.txtCount);
            txtCategory=itemView.findViewById(R.id.textView56);
            txtProductCount=itemView.findViewById(R.id.textView57);

        }
    }

    public void setDialogResponse(Listener dialogResponse){
        this.listener=dialogResponse;
    }

    public interface Listener{
        void edit(Stok stok);
        void delete(Stok stok);
    }
}
